#!/bin/bash 

cd "$(dirname "$0")"

docker run -it -v ${PWD}/..:${PWD}/.. zarcastic/docker_images:cpp /bin/bash ${PWD}/.build.sh